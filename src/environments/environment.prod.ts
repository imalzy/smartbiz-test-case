export const environment = {
  production: true,
  AUTH_URL: ' https://api.smartbiz.id/api/login',
  BASE_URL: ' https://api.smartbiz.id/api/demo/',
  REFRESH_TOKEN: 'https://api.smartbiz.id/api/refresh-token',
};
