import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { Employee, Row } from '../../model/employee';
import { map, catchError } from 'rxjs/operators';
import { ObserveOnMessage } from 'rxjs/internal/operators/observeOn';

@Injectable({
  providedIn: 'root',
})
export class EmployeeService {
  constructor(private http: HttpClient) {}

  getEmployeeId(id = 10): Promise<any> {
    return this.http.get(`${environment.BASE_URL}/employee/${id}`).toPromise();
  }

  all(
    value: string,
    startBirthDate: string,
    endBirthDate: string,
    page: number,
    perpage: number,
    sort: string,
    field: String
  ): Observable<Employee> {
    let startBirthdates: string;
    let endBirthdates: string;

    if (startBirthDate === null) {
      startBirthdates = '1982-10-10 00:00:00';
    }

    if (endBirthDate === null) {
      endBirthdates = '2020-10-10 00:00:00';
    }

    const body = {
      query: {
        value: value,
      },
      start_birth_date: startBirthdates,
      end_birth_date: endBirthdates,
      pagination: {
        page: page,
        perpage: perpage,
      },
      sort: {
        sort: sort,
        field: field,
      },
    };
    return this.http
      .post<Employee>(`${environment.BASE_URL}/employee/page-search`, body)
      .pipe(map((employee: Employee) => employee));
  }

  edit(id: number, employee: Row): Observable<any> {
    return this.http.put(`${environment.BASE_URL}/employee/${id}`, employee);
  }

  create(employee: Row): Observable<any> {
    return this.http.post(`${environment.BASE_URL}/employee`, employee);
  }

  delete(id: number): Observable<any> {
    return this.http.delete(`${environment.BASE_URL}/employee/${id}`);
  }

  byId(id: number): Observable<any> {
    return this.http
      .get(`${environment.BASE_URL}/employee/${id}`)
      .pipe(map((employee: Row) => employee));
  }
}
