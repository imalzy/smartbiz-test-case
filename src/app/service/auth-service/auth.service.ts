import { Iuser, Itoken, IuserClaims, Api, UserToken } from '../../model/user';
import { Injectable } from '@angular/core';
import { Router, Data } from '@angular/router';
import { BehaviorSubject, from, Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { map, tap, mapTo, catchError } from 'rxjs/operators';

@Injectable()
export class AuthService {
  private loggedInHeader: BehaviorSubject<boolean> = new BehaviorSubject<
    boolean
  >(false);

  private readonly JWT_TOKEN = 'JWT_TOKEN';
  private readonly REFRESH_TOKEN = 'REFRESH_TOKEN';
  private loggedUser: string;

  constructor(private _router: Router, private http: HttpClient) {}

  isLoggedIn() {
    return !!this.getJwtToken();
  }

  get LoggedInHeader() {
    return this.loggedInHeader.asObservable();
  }

  // login(user: Iuser): Observable<boolean> {
  //   return this.http.post<Api<UserToken>>(`${environment.AUTH_URL}`, user).pipe(
  //     tap((res) => {
  //       this.doLoginUser(res.data.user, res.data.token);
  //       this._router.navigate(['/employees']);
  //     }),
  //     mapTo(true),
  //     catchError((error) => {
  //       return of(false);
  //     })
  //   );
  // }

  login(user: Iuser): Observable<any> {
    return this.http.post<Api<UserToken>>(`${environment.AUTH_URL}`, user).pipe(
      map((res) => {
        if (res.status === 200) {
          this.doLoginUser(res.data.user, res.data.token);
          this.loggedInHeader.next(true);
          this._router.navigate(['/employees']);
        }
      })
    );
  }

  private doLoginUser(user: IuserClaims, token: Itoken) {
    this.loggedUser = user.username;
    this.storeUserData(user);
    this.storeTokens(token);
  }

  getJwtToken() {
    return localStorage.getItem(this.JWT_TOKEN);
  }

  private getRefreshToken() {
    return localStorage.getItem(this.REFRESH_TOKEN);
  }

  private storeJwtToken(jwt: string) {
    localStorage.setItem(this.JWT_TOKEN, jwt);
  }

  getUserData() {
    return JSON.parse(localStorage.getItem('USER'));
  }

  private storeUserData(user) {
    localStorage.setItem('USER', JSON.stringify(user));
  }

  refreshToken() {
    return this.http
      .post<any>(`${environment.REFRESH_TOKEN}`, {
        // refreshToken: this.getRefreshToken(),
      })
      .pipe(
        tap((tokens: Itoken) => {
          this.storeJwtToken(tokens.access_token);
        })
      );
  }

  private storeTokens(tokens: Itoken) {
    localStorage.setItem(this.JWT_TOKEN, tokens.access_token);
    localStorage.setItem(this.REFRESH_TOKEN, tokens.refresh_token);
  }

  private removeTokens() {
    localStorage.clear();
  }

  logout() {
    this.loggedUser = null;
    this.removeTokens();
    this.loggedInHeader.next(false);
    this._router.navigate(['/login']);
  }
}
