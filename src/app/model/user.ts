export interface Iuser {
  identity: string;
  password: string;
}

export interface Itoken {
  access_token: string;
  refresh_token: string;
  token_type: string;
}

export interface IuserClaims {
  applications: any;
  companies: any;
  email: any;
  groups: any;
  roles: any;
  username: string;
}

export interface UserToken {
  token: Itoken;
  user: IuserClaims;
}

export class Api<T> {
  data: T;
  message: string;
  status: number;
}
