export interface Row {
  address: string;
  birth_date: Date;
  created_by: string;
  email: string;
  full_name: string;
  id: number;
  mobile: string;
  modified_by: string;
  nick_name: string;
  nip: string;
  phone: string;
}

export interface Employee {
  meta: {
    page: number;
    pages: number;
    perpage: number;
    total: number;
    sort: string;
    field: string;
  };
  rows: Row[];
}
