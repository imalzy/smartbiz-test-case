import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '@service/auth-service/auth.guard';
import { AppGuard } from '@service/auth-service/app.guard';

import { AppLayoutComponent } from './layout/app-layout.component';
import { LoginLayoutComponent } from './layout/login-layout.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: '',
    children: [
      {
        path: 'login',
        component: LoginLayoutComponent,
        canActivate: [AuthGuard],
        loadChildren: () =>
          import('./main/login/login.module').then((m) => m.LoginModule),
      },
      {
        path: 'employees',
        component: AppLayoutComponent,
        canActivate: [AppGuard],
        canLoad: [AppGuard],
        loadChildren: () =>
          import('./main/main.module').then((m) => m.MainModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
