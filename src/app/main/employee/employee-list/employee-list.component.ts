import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '@service/employee-service/employee.service';
import { PageEvent } from '@angular/material/paginator';
import { Employee } from 'src/app/model/employee';
import { map } from 'rxjs/operators';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { EmployeeFormComponent } from '../employee-form/employee-form.component';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css'],
})
export class EmployeeListComponent implements OnInit {
  displayedColumns = [
    'nip',
    'full_name',
    'email',
    'birth_date',
    'address',
    'action',
  ];

  dataSource: Employee;
  filterValue: string;
  pageEvent: PageEvent;
  page: number = 1;
  size: number = 2;

  constructor(
    private employeeService: EmployeeService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.refreshData();
  }

  refreshData() {
    this.employeeService
      .all(null, null, null, this.page, this.size, 'ASC', 'id')
      .pipe(map((res: Employee) => (this.dataSource = res)))
      .subscribe();
  }

  onPaginateChange(event: PageEvent) {
    this.page = event.pageIndex + 1;
    this.size = event.pageSize;

    if (this.filterValue === null) {
      this.employeeService
        .all(null, null, null, this.page, this.size, 'ASC', 'id')
        .pipe(map((res: Employee) => (this.dataSource = res)))
        .subscribe();
    } else {
      this.refreshData();
    }
  }

  onEdit(id: number) {
    const dialogRef = this.dialog.open(EmployeeFormComponent, {
      panelClass: 'dialog',
      hasBackdrop: true,
      disableClose: true,
      width: '40%',
      data: id,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if(result.event === 'save'){
        this.refreshData();
      }
    });
  }

  onDelete(id: number) {
    var r = confirm('Delete ?');
    if (r == true) {
      console.log(id);
      this.employeeService.delete(id).subscribe((res) => {
        console.log(res);
        if (res.status === 200) {
          alert('Delete Success');
          this.refreshData();
        }
      });
    }
  }
}
