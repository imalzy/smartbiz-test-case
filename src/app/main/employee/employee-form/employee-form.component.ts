import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl } from '@angular/forms';
import { EmployeeService } from '@service/employee-service/employee.service';

@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.css'],
})
export class EmployeeFormComponent implements OnInit {
  form: FormGroup;
  action: string;
  constructor(
    private employeeService: EmployeeService,
    public dialogRef: MatDialogRef<EmployeeFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit(): void {
    console.log(this.data);
    if (this.data === 0) {
      this.action = 'new';
    } else {
      this.action = 'edit';
      this.employeeService.byId(this.data).subscribe((res) => {
        // console.log(res)
        // this.form.patchValue(res)
        this.form.get('nip').patchValue(res.nip);
        this.form.get('full_name').patchValue(res.full_name);
        this.form.get('nick_name').patchValue(res.nick_name);
        this.form
          .get('birth_date')
          .patchValue(
            res.birth_date !== null ? new Date(res.birth_date) : res.birth_date
          );
        this.form.get('address').patchValue(res.address);
        this.form.get('phone').patchValue(res.phone);
        this.form.get('mobile').patchValue(res.mobile);
        this.form.get('email').patchValue(res.email);
      });
    }

    this.reactiveForm();
  }

  reactiveForm() {
    this.form = new FormGroup({
      nip: new FormControl(''),
      full_name: new FormControl(''),
      nick_name: new FormControl(''),
      birth_date: new FormControl(''),
      address: new FormControl(''),
      phone: new FormControl(''),
      mobile: new FormControl(''),
      email: new FormControl(''),
    });
  }

  onSave() {
    console.log(this.form);
    if (this.action === 'new') {
      this.employeeService.create(this.form.value).subscribe((res) => {
        if (res.status === 200) {
          this.dialogRef.close({ event: 'save' });
        }
      });
    } else {
      this.employeeService.edit(this.data, this.form.value).subscribe((res) => {
        if (res.status === 200) {
          this.dialogRef.close({ event: 'save' });
        }
      });
    }
  }
}
