import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '@service/auth-service/auth.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  errorMessage: string;

  reactiveForm() {
    this.loginForm = new FormGroup({
      identity: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
    });
  }

  constructor(private authService: AuthService) {}

  ngOnInit(): void {
    this.reactiveForm();
  }

  login() {
    this.authService
      .login(this.loginForm.value)
      .pipe(first())
      .subscribe(
        (res) => {
          return;
        },
        (err) => {
          console.log(err);
          this.errorMessage = err.error.message;
        }
      );
  }
}
