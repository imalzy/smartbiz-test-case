import { Component, OnInit } from '@angular/core';
import { AuthService } from '@service/auth-service/auth.service';
import { IuserClaims } from '../model/user';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  user: IuserClaims;
  isLoggedIn$: Observable<boolean>;

  constructor(private authService: AuthService) {}

  ngOnInit(): void {
    this.user = this.authService.getUserData();
    this.isLoggedIn$ = this.authService.LoggedInHeader;
  }

  logout() {
    this.authService.logout();
  }
}
